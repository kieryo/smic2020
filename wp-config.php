<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'smic_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Dwz}YWJ.D_H^NH|u6B%5?}D#oN%LLWg=)41_mb2ZL<dhev7S2~!6[h+i|m; cwd@' );
define( 'SECURE_AUTH_KEY',  'Y]lIDQ@s5&&l:X-@-[OR4b|7tfR.FVNx{>6f`d+;x|)>~M4 b$UR>.y]:2yF|1iH' );
define( 'LOGGED_IN_KEY',    'JTWq#rJE}xJy$`1p12B5A.oKVChRjAR:8K)VplPx/rwq 3 o.uC%@>$OC?=8d&E]' );
define( 'NONCE_KEY',        'DH-kP4Fn:dMMi,AGF`sq~*k}F2A,gMQb}y0nk*xz:YE =%3OT4E/6PKEwBge;Ipc' );
define( 'AUTH_SALT',        '=uu9xA.#pDM(q3~C.%l>n^?X}wlc4>A<)3=l)_V0+K]P9gEZ^:Lx0B,Hsmg2pN|s' );
define( 'SECURE_AUTH_SALT', '%[,s5_5Xs.DsA^DRkXZt;FFR[Sx) 0,Y5._PcD|z7Sp5Y3RC=*-JO3>&zyUsg1;J' );
define( 'LOGGED_IN_SALT',   '?C`R#Ei5/l<hFr`q<Nn(LK:Y?^NW2kk{|D&*YUGiY?*b?-cbF)pms-HxepFPNEyS' );
define( 'NONCE_SALT',       ':wQZH4PxHro)FT:y|:8lxUE15fl9Qhq^&zCr,b] $CIBKLdP$12 +|,(l$!LDnZ)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'smic_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
