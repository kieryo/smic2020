<?php

	/* DISPLAY Board of Directors */
	$cpt_arg = array(
		'post_type' => 'board_directors', 
		'post_status' => 'publish', 
  	'posts_per_page' => -1,
		'order' => 'DESC'
	);
  $cpt_query = new WP_Query($cpt_arg);

?>

<div class="filter-bod-container">
	<select name="filter_bod" id="filter_bod" class="form-control filter-bod">
		<option value="bod_all">Show All</option>
		<?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post();  ?>
			<option value="bod_<?= get_the_ID(); ?>"><?= the_title(); ?></option>
		<?php endwhile; endif; ?>
	</select>
</div>

<section class="smic-wrapper board-of-directors">
	<div class="smic-inner-container">
		<div class="section-body">
			<div class="bod-container">
				<?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post();  ?>
					<?php $bod_position = get_post_meta(get_the_ID(),'bod_position'); ?>
					<?php $bod_age      = get_post_meta(get_the_ID(),'bod_age'); ?>

					<div id="bod_<?= get_the_ID(); ?>" class="bod-wrapper">
						<div class="title-wrapper">
							<h4 class="bod-name"><?= the_title(); ?></h4>
						</div>
						<div class="body-wrapper">
							<div class="bod-details">
								<div class="bod-info">
									<p class="position"><?= $bod_position[0]; ?></p>
									<?php if($bod_age[0]): ?>
									<p class="age"><?= $bod_age[0]; ?> Years Old</p>
									<?php endif; ?>
								</div>
									<?= the_content(); ?>
							</div>
							<div class="bod-img">
								<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
									<?php the_post_thumbnail('full'); ?>
								<?php } ?> 
							</div>

						</div>
					</div>
			  <?php endwhile; endif; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>