
<?php $fields = get_fields(); ?>
<?php //print'<pre>';print_r($fields);print'</pre>'; ?>

<?php if(!$_GET['paged_ac']): ?>

	<?php if(have_posts() ) : ?>
		<?php while( have_posts() ) : the_post(); ?>
			<section class="mainbody-content">
				<div class="smic-inner-subcontainer">
					<?= the_content(); ?>
				</div>
			</section>
		<?php endwhile; ?>
	<?php endif; ?>


	<?php if($fields['forbes_section']): ?>
		<section class="smic-wrapper about-forbes-section">
			<div class="smic-inner-subcontainer">
				<?= $fields['forbes_section']['description']; ?>
				<div class="imgs-wrapper">
					<?php foreach($fields['forbes_section']['images'] as $values): ?>
						<img src="<?= $values['image']['url']; ?>" alt="<?= $values['image']['alt']; ?>">
					<?php endforeach; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>

<?php endif; ?>


<?php
	/* DISPLAY AWARDS AND CITATIONS */
	$paged_ac = isset( $_GET['paged_ac'] ) ? (int) $_GET['paged_ac'] : 1;
	$cpt_arg = array(
		'post_type' => 'award_citation', 
		'post_status' => 'publish', 
  	'posts_per_page' => 12,
  	'order_by' => 'date',
  	'paged' => $paged_ac,
		'order' => 'DESC'
	);
  $cpt_query = new WP_Query($cpt_arg);
?>


	<section class="smic-wrapper awards-citations-container">
		<div class="smic-inner-subcontainer">
		<?php if ($cpt_query->have_posts()):  ?>
			<?php while ($cpt_query->have_posts()) : $cpt_query->the_post();  ?>
	      <div class="ac-wrapper">
	          <div class="ac-wrapper-inner">
	              <div class="img-wrapper">
									<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
										<?php the_post_thumbnail('full'); ?>
									<?php } ?> 						
	              </div>
	              <div class="title"><?= the_title(); ?></div>
	              <div class="info"><?= the_content(); ?></div>
	          </div>
	      </div>
			<?php endwhile; ?>
		<?php endif; ?>

		<div class="pagination">
		    <?php 
          // http://codex.wordpress.org/Class_Reference/WP_Query#Pagination_Parameters
          $pag_args1 = array(
							'prev_text'    => __('<i class="fa fa-chevron-left"></i> Prev'),
							'next_text'    => __('Next <i class="fa fa-chevron-right"></i>'),
              'format'  => '?paged_ac=%#%',
              'current' => $paged_ac,
              'total'   => $cpt_query->max_num_pages,
          );
          echo paginate_links( $pag_args1 );
		    ?>
		</div>

		<?php wp_reset_postdata(); ?>
			
		</div>
	</section>