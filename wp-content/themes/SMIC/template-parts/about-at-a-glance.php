
<?php $fields = get_fields(); ?>
<?php //sprint'<pre>';print_r($fields);print'</pre>'; ?>

<section class="smic-wrapper smic-graphs-container view-all-wrapper">
	<div class="smic-inner-subcontainer">

		<div class="smic-graphs core-investments">
			<div class="section-title">
				<h3 class="title">Core Investments</h3>
			</div>
			<div class="section-body">

				<div class="pie-chart-wrapper">
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-77">
								<div class="pie-details">
									<p class="pie-info counter">77.3%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">SM Retail <br/>Inc.</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-50">
								<div class="pie-details">
									<p class="pie-info counter">49.7%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">SM Prime <br/>Holdings</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-45">
								<div class="pie-details">
									<p class="pie-info counter">45.3%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">BDO <br/>Unibank</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-23">
								<div class="pie-details">
									<p class="pie-info counter">22.6%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">China Banking <br/>Corporation</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="smic-graphs equity-investments">
			
			<div class="section-title">
				<h3 class="title">Equity Investments</h3>
			</div>
			<div class="section-body">

				<div class="pie-chart-wrapper">
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-27">
								<div class="pie-details">
									<p class="pie-info counter">26.4%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">Belle Corp.</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-35">
								<div class="pie-details">
									<p class="pie-info counter">34.1%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">Atlas Mining</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-34">
								<div class="pie-details">
									<p class="pie-info counter">34.0%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">CityMall</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-31">
								<div class="pie-details">
									<p class="pie-info counter">30.5%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">2GO Group</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-64">
								<div class="pie-details">
									<p class="pie-info counter">63.3%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">MyTown</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-95">
								<div class="pie-details">
									<p class="pie-info counter">95.0%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">NEO Subsidiaries</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-34">
								<div class="pie-details">
									<p class="pie-info counter">34.0%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">NEO Associates</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-35">
								<div class="pie-details">
									<p class="pie-info counter">35.0%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">AirSpeed</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-35">
								<div class="pie-details">
									<p class="pie-info counter">34.5%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">GrabPay</h4>
					</div>
					<div class="pie-size">
						<div class="pie-bg-color">
							<div class="pie-wrapper progress-35">
								<div class="pie-details">
									<p class="pie-info counter">34.1%</p>
								</div>
								<div class="pie">
									<div class="left-side half-circle"></div>
									<div class="right-side half-circle"></div>
								</div>
								<div class="shadow"></div>
							</div>
						</div>
						<h4 class="title">Goldilocks</h4>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

<?php if($fields['vision_mission_image']):  ?>
<section class="smic-wrapper contribution-to-ndg at-a-glance-parallax parallax" style="background-image: url(<?= $fields['vision_mission_image']['url']; ?>)">
	<div class="parallax-content">
		<div class="smic-inner-subcontainer">
			<div class="row">
				<div class="left-content col-md-4">
					<h3 class="title mb-2">Vision</h3>
					<p><?= $fields['vision_and_mission']['vision']; ?></p>
				</div>
				<div class="right-content col-md-8">
					<h3 class="title mb-2">Mission</h3>
					<p><?= $fields['vision_and_mission']['mission']; ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>